# Third party source files

## Moment - time-zone manipulation

- https://momentjs.com/downloads/moment-timezone-with-data.js
- https://momentjs.com/downloads/moment-with-locales.js

## OpenLayers - map display

- https://openlayers.org/
- https://github.com/openlayers/openlayers/releases/download/v6.4.3/v6.4.3.zip
- https://github.com/openlayers/openlayers/

- lib/ol.css
- lib/ol.js

Not entirely part of openlayers, but conglomerated together from other stuff:

- lib/shadow.js

## Other Third Party routines

lib/third-party.js
