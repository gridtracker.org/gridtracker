const nodeTimers = require("timers");
const dns = require("node:dns");

try
{
  dns.setDefaultResultOrder("ipv4first");
  dns.promises.setDefaultResultOrder("ipv4first");
}
catch (e)
{
  console.log("Can't set dns IPv4 default order");
}

// GridTracker object
var GT = {};
// CallRoster object
var CR = {};

var isShowing = false;
var s_title = null;
var s_screenSettings = {};
var s_zoomLevel = 0;

nw.Screen.Init();

// Look for our own settings file, if not, we'll rely on GT.localStorage being accurate
if (document.title.substring(0, 12).trim() == "GridTracker")
{
  const path = require("path");
  const fs = require("fs");
  let filename = path.join(path.join(nw.App.dataPath, "Ginternal"), "settings.json");
  try
  {
    if (fs.existsSync(filename))
    {
      let data = require(filename);
      if (data)
      {
        GT.localStorage = data;
      }
      else
      {
        // safety catch
        GT.localStorage = {};
        for (var key in localStorage)
        {
          GT.localStorage[key] = localStorage[key];
        }
      }
    }
    else
    {
      // This should happen only once for new users
      GT.localStorage = {};
      for (var key in localStorage)
      {
        GT.localStorage[key] = localStorage[key];
      }
    }
  }
  catch (e)
  {
    GT.localStorage = {};
    for (var key in localStorage)
    {
      GT.localStorage[key] = localStorage[key];
    }
    console.log(e);
  }
}
else
{
  GT.localStorage = window.opener.GT.localStorage;
}

var g_screenLost = false;
var g_windowInfo = {};
var g_initialScreenCount = nw.Screen.screens.length;

function setWindowInfo()
{
  // if we've lost a screen, stop saving our info
  if (g_screenLost) return;
  var win = nw.Window.get();
  var windowInfo = {};

  windowInfo.x = win.x;
  windowInfo.y = win.y;
  windowInfo.width = win.width;
  windowInfo.height = win.height;
  g_windowInfo = windowInfo;
}

function clearAllScreenTimers()
{
  if (g_windowMoveTimer != null)
  {
    nodeTimers.clearTimeout(g_windowMoveTimer);
    g_windowMoveTimer = null;
  }
  if (g_windowResizeTimer != null)
  {
    nodeTimers.clearTimeout(g_windowResizeTimer);
    g_windowResizeTimer = null;
  }
}

var screenCB = {
  onDisplayAdded: function (screen)
  {
    clearAllScreenTimers();
    if (
      g_screenLost == true &&
      g_initialScreenCount == nw.Screen.screens.length
    )
    {
      // Lets restore the position now
      var win = nw.Window.get();
      win.x = g_windowInfo.x;
      win.y = g_windowInfo.y;
      win.width = g_windowInfo.width;
      win.height = g_windowInfo.height;
      g_screenLost = false;
    }
  },

  onDisplayRemoved: function (screen)
  {
    clearAllScreenTimers();
    if (g_initialScreenCount != nw.Screen.screens.length)
    {
      g_screenLost = true;
    }
  }
};

function saveScreenSettings()
{
  setWindowInfo();

  var setting = { showing: isShowing, zoomLevel: s_zoomLevel, window: g_windowInfo };
  s_screenSettings = JSON.parse(GT.localStorage.screenSettings);
  s_screenSettings[s_title] = setting;
  GT.localStorage.screenSettings = JSON.stringify(s_screenSettings);
}
// listen to screen events
nw.Screen.on("displayAdded", screenCB.onDisplayAdded);
nw.Screen.on("displayRemoved", screenCB.onDisplayRemoved);

nw.Window.get().on("loaded", function ()
{
  // Use the first 12 bytes of the title(trimmed) as storage names
  // This cannot be changed as current installs (38,000+) use this naming convention
  s_title = document.title.substr(0, 12).trim();
  isShowing = false;
  if (typeof GT.localStorage.screenSettings == "undefined")
  {
    GT.localStorage.screenSettings = "{}";
  }
  s_screenSettings = JSON.parse(GT.localStorage.screenSettings);

  if (!(s_title in s_screenSettings))
  {
    saveScreenSettings();
  }
  if (!("zoomLevel" in s_screenSettings[s_title]))
  {
    saveScreenSettings();
  }
  if (!("window" in s_screenSettings[s_title]))
  {
    saveScreenSettings();
  }
  isShowing = s_screenSettings[s_title].showing;
  nw.Window.get().zoomLevel = s_zoomLevel = s_screenSettings[s_title].zoomLevel;

  g_windowInfo = s_screenSettings[s_title].window;

  var win = nw.Window.get();
  win.x = g_windowInfo.x;
  win.y = g_windowInfo.y;
  win.width = g_windowInfo.width;
  win.height = g_windowInfo.height;

  // Check the first part of the string, only one window has "GridTracker" in the name.
  // It is reserved to the main app window.
  if (isShowing || s_title.indexOf("GridTracker") == 0)
  {
    this.show();
  }
  else
  {
    this.hide();
  }

  g_initialScreenCount = nw.Screen.screens.length;
 
  setWindowInfo();
  document.addEventListener("keydown", onZoomControlDown, true);
});

var g_windowMoveTimer = null;
nw.Window.get().on("move", function (x, y)
{
  if (g_windowMoveTimer != null)
  {
    nodeTimers.clearTimeout(g_windowMoveTimer);
  }
  g_windowMoveTimer = nodeTimers.setTimeout(setWindowInfo, 1000);
});

var g_windowResizeTimer = null;
nw.Window.get().on("resize", function (w, h)
{
  if (g_windowResizeTimer != null)
  {
    nodeTimers.clearTimeout(g_windowResizeTimer);
  }
  g_windowResizeTimer = nodeTimers.setTimeout(setWindowInfo, 1000);
});

var g_zoomKeys = {
  NumpadSubtract: reduceZoom,
  Minus: reduceZoom,
  NumpadAdd: increaseZoom,
  Equal: increaseZoom,
  Numpad0: resetZoom,
  Digit0: resetZoom
};

function onZoomControlDown(event)
{
  if (event.ctrlKey)
  {
    if (event.code in g_zoomKeys)
    {
      g_zoomKeys[event.code]();
    }
  }
}

function reduceZoom()
{
  s_zoomLevel -= 0.2;
  nw.Window.get().zoomLevel = s_zoomLevel;
  saveScreenSettings();
}

function increaseZoom()
{
  s_zoomLevel += 0.2;
  nw.Window.get().zoomLevel = s_zoomLevel;

  saveScreenSettings();
}

function resetZoom()
{
  s_zoomLevel = 0;
  nw.Window.get().zoomLevel = s_zoomLevel;
  saveScreenSettings();
}

function lockNewWindows()
{
  var gui = require("nw.gui");
  var win = gui.Window.get();
  win.on("new-win-policy", function (frame, url, policy)
  {
    nw.Shell.openExternal(url);
    policy.ignore();
  });
}

process.on("uncaughtException", function (e) {});
